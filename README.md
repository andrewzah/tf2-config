# 앤드루's configs

Based on [RaysHud](https://github.com/raysfire/rayshud) & [Mastercomfig](https://github.com/mastercoms/mastercomfig)

### General modifications
* Crosshair flickers between purple/green when pressing WASD (from stabby's configs)
* concise voice line menu
* Null canceling script
* Use custom cursors per class per gun
* Custom hitsound (from woolen sleevelet)
* Custom killsound (roblox)
* different `cl_interp` per class (from stabby's configs)
* Network settings
* Press `]` to suicide
* protection from server / motd
* tab activates scoreboard, netgraph, and `mem_compact`
* colorblind assist on

### Spy Modifications
* Q toggles between the sapper & knife
  * If using a weapon other than the sapper, Q always goes sapper first

### Concise Voice Menu
This operates like spy's concise menu.

| Z | Phrase |
| :---: | :---: |
| zzz | medic |
| zzc | go go go |
| zxz | move up |
| zzx | thanks |
| zcz | yes |
| zcx | no |
| zxc | go right |
| zxx | go left |

| X | Phrase |
| :---: | :---: |
| xzz | incoming |
| xzx | spy |
| xxx | need a dispenser here |
| xxz | teleporter here |
| xxc | sentry here |
| xcz | activate charge |
| xcx | ubercharge ready (medic only) |
  
| C | Phrase |
| :---: | :---: |
 | czz | help |
 | czx | battle cry |
 | czc | cheers |
 | cxz | jeers |
 | cxx | positive |
 | cxc | negative |
 | ccz | nice shot |
 | ccx | good job |
